from tools.console import *
import random
#5.4.1
def affichageList():
    clear()
    print("Trois façons différentes d'afficher le contenu d'une liste\n\
(deux méthodes avec for et une avec while)")
    etreVivant = ["vache", "souris", "levure","bacterie"]
    print(f"etreVivant = {etreVivant}")
    print()
    print("la première boucle for")
    print("for etre in etreVivant :\n\
    print(etre)")
    print("Affichage :")
    for etre in etreVivant :
        print(etre)
    
    print()
    print("la deuxième boucle for")
    print("for etre in etreVivant[0:4]:\n\
    print(etre)")
            
    print("Affichage :")
    for etre in etreVivant[0:4]:
        print(etre)
    
    print()
    print("la boucle while")

    print("i=0\n\
while i<len(etreVivant) :\n\
    print(etreVivant[i])\n\
        i=i+1")
    print("Affichage :")
    i=0
    while i<len(etreVivant) :
        print(etreVivant[i])
        i=i+1
    entrerContinuer()
    clear()
#5.4.2
def boucleSemaine():
    clear()
    print("Boucle et jours de la semaine")
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    print(f"semaine = {semaine}")
    print()
    print("Boucle for pour afficher les jours de la semaine")
    print("for jour in semaine :\n\
    print(jour)")
    for jour in semaine :
        print(jour)
    print()
    print("Boucle while pour afficher les jours du weekend")

    print("i=5\n\
while i<len(semaine):\n\
    print(semaine[i])\n\
    i=i+1")
    i=5
    while i<len(semaine):
        print(semaine[i])
        i=i+1
    entrerContinuer()
    clear()
#5.4.3
def unAdixMemeLigne():
    clear()
    print("Affichage de nombres 1 à 10 sur une même ligne avec la boucle for")
    print("for i in range(0, 11):\n\
    print(i, end=" ")")
    for i in range(0, 11):
        print(i, end=" ")
    entrerContinuer()
    clear()
#5.4.4
def pairsImpairs():
    clear()
    print("construction d'une liste pairs dans laquelle\
tous les éléments de impairs sont incrémentés de 1")
    print()
    impairs = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
    print(f"impairs = {impairs}")
    print("pairs=[]\n\
for i in impairs :\n\
    pairs = pairs + [i+1]\n\
    i=i+1")
    print()
    pairs=[]
    for i in impairs :
        pairs = pairs + [i+1]
        i=i+1
    print(f"pairs = {pairs}")
    entrerContinuer()
    clear()
#5.4.5
def calculMoyenne():
    clear()
    print("Calcul de la moyenne avec écriture formatée et deux décimales")
    note=[14, 9, 6, 8, 12]
    print()
    print(f"Note de l'étudiant = {note}")
    print("Calcul de la moyenne avec la boucle for")
    print("moyenne = 0\n\
for i in note :\n\
    moyenne += i / len(note)")
    moyenne = 0
    for i in note :
        moyenne += i / len(note)
    print("Affichage :")
    print("print(f\"{moyenne:.2f}\")")
    print(f"{moyenne:.2f}")
    entrerContinuer()
    clear()
#5.4.6
def produitsConsecutifs():
    clear()
    print("Produit de nombres consécutifs avec les fonctions range() et list()")
    print("Création de la liste des nombres pairs entre 2 et 20 inclus :")

    print("pair=list(range(2,21,2))")
    pair=list(range(2,21,2))
    print(f"pair = {pair}")
    print("Calcul et affichage des produits :")
    print("i=0\n\
while i<len(pair) -1 :\n\
    print(f\"{pair[i]*pair[i+1]}\")\n\
    i=i+1")
    print("Affichage")
    i=0
    while i<len(pair) -1 :
        print(f"{pair[i]*pair[i+1]}")
        i=i+1
    entrerContinuer()
    clear()
#5.4.7
def triangle():
    clear()
    print("Un script qui dessine un triangle :")
    print("etoile = \"*\"\n\
for i in range(0,8):\n\
    print(etoile)\n\
    etoile += \"*\"")
    etoile = "*"
    for i in range(0,8):
        print(etoile)
        etoile += "*"
    entrerContinuer()
    clear()
#5.4.8
def triangleInverse():
    clear()
    print("Un script qui dessine un triangle inversé :")
    print("etoile = \"*\"\n\
for i in range(10,0,-1):\n\
    print(etoile*i)")
    etoile = "*"
    for i in range(10,0,-1):
        print(etoile*i)
    entrerContinuer()
    clear()
#5.4.9
def triangleGauche():
    clear()
    print("Un script qui dessine un triangle gauche :")
    print("etoile = \"*\"\n\
espace = \" \"\n\
for i in range(10,0,-1):\n\
    print(espace*(i-1) + etoile*(11-i))")
    etoile = "*"
    espace = " "
    for i in range(10,0,-1):
        print(espace*(i-1) + etoile*(11-i))
    entrerContinuer()
    clear()
#5.4.10
def pyramide():
    clear()
    print("Un script qui dessine un pyramide :")
    print("etoile = \"*\"\n\
espace = \" \"\n\
reponse = input(\"Entrer le nombre de lignes pour le pyramide (entier positif) : \")\n\
lignes = int(reponse)\n\
for i in range(lignes):\n\
    print(espace*(lignes-i) + etoile*(i*2+1))")
    
    etoile = "*"
    espace = " "
    reponse = input("Entrer le nombre de lignes pour le pyramide (entier positif) : ")
    lignes = int(reponse)
    for i in range(lignes):
        print(espace * (lignes - i) + etoile * (i * 2 + 1))
    entrerContinuer()
    clear()
#5.4.11
def parcoursMatrice():
    clear()
    print("Un script qui parcours une matrice")
    def parcoursFor(x):
        print("Affichage avec la Boucle for d'une matrice de taille : ", x)
        print("ligne colonne")

        taille = x
        for i in range(taille):
            for j in range(taille):
                print(f"{i+1:>4d}{j+1:>5d}")
    def parcoursWhile(x):        
        print("Affichage avec la boucle while d'une matrice de taille : ", x)
        print("ligne colonne")
        taille = x
        k = 0
        while k < taille :
            l=0
            while l< taille :
                l+=1
                print(f"{k+1:>4d}{l:>5d}")
            k+=1
    print()
    print("Parcours de matrice de taille 2")
    parcoursFor(2)
    print()
    parcoursWhile(2)
    print()
    print("Parcours de matrice de taille 3")
    parcoursFor(3)
    print()
    parcoursWhile(3)
    print()
    print("Parcours de matrice de taille 5")
    parcoursFor(5)
    print()
    parcoursWhile(5)
    print()
    print("Parcours de matrice de taille 10")
    parcoursFor(10)
    print()
    parcoursWhile(10)
    entrerContinuer()
    clear()
#5.4.12
def affichageDemiMatrice():
    clear()
    def parcoursDemiMatrice(x):
        print("Affichage demi-matrice")
        print(f"N = {x}")
        N = x
        caseParcourues=0
        print("ligne colone")
        for i in range(N-1):
            for j in range(i+1,N):
                print(f"{i+1:>4d}{j+1:>5d}")
                caseParcourues += 1

        print(f"Pour une matrice {N}x{N}, on a parcouru {caseParcourues} cases")
        print()
    
    def nombreCasesDemiMatrice(x):
        N = x
        caseParcourues=0
        for i in range(N-1):
            for j in range(i+1,N):
                caseParcourues += 1

        print(f"Si N = {N}; {caseParcourues} cases parcourues ")
    
    parcoursDemiMatrice(3)
    parcoursDemiMatrice(4)
    parcoursDemiMatrice(5)
    for i in range(2,11):
        nombreCasesDemiMatrice(i)
    print()
    print("La formule générale reliant le nombre de cases parcourues à N est :\n")
    print("Nombre total de cases parcourues = [N x (N-1)]/2")
    print("Exemple :\nN = 5\nNombre total de cases parcourues = (5 x 4)/2 = 10")
    entrerContinuer()
    clear()
#5.4.13
def sautDePuce():
    clear()
    print("Une puce qui se déplace aléatoirement sur une ligne, en avant ou en arrière, par pas de 1 ou -1")
    print()
    import random

    # Position initiale de la puce
    position = 0
    print(position, end=" ")
    # Emplacement final de la puce
    destination = 5

    # Nombre de sauts nécessaires pour réaliser le parcours
    sauts = 0

    #Condition de sortie de la boucle while
    sautsMax = 100000

    while position != destination and sauts < sautsMax :
        # Incrémentation du nombre de sauts
        sauts += 1
        
        # Déplacement de la puce : on utilise random.choice pour choisir au hasard parmi -1 et 1
        deplacement = random.choice([-1, 1])
        
        # Mise à jour de la position de la puce
        position += deplacement

        print(f"{position}", end=" ")

        #Affichage si le nombre de saut dépasse sautMax
    if sautsMax<= sauts :

        print(f"\nle nombre de sauts a excedé {sautsMax}")
    else :
        print(f"\nLe parcours de la puce à l'emplacement {destination} a nécessité {sauts} sauts.")
    entrerContinuer()
    clear()
#5.4.14
# Fonction pour construire les premiers termes de la suite de Fibonacci
def fibonacci():
    clear()
    print("def fibonacci(n):\n\
    fibo = [0, 1]\n\
    while len(fibo) < n:\n\
        fibo.append(fibo[-1] + fibo[-2])\n\
    return fibo")
    def affichageFibo(n):
        fibo = [0, 1]
        while len(fibo) < n:
            fibo.append(fibo[-1] + fibo[-2])
        return fibo

    # Construire la liste fibo avec les 15 premiers termes
    fibo = affichageFibo(15)

    # Afficher la liste fibo
    print()
    print("Liste 15 premiers termes de Fibonacci: ", fibo)

    # Afficher le rapport entre chaque élément de rang n et l'élément de rang n-1 (pour n > 1)
    print()
    print("Rapports:")
    for i in range(2, len(fibo)):
        ratio = fibo[i] / fibo[i-1]
        print(f"Rapport entre {fibo[i]} et {fibo[i-1]} : {ratio}")
    print()
    print("à mesure que les termes augmentent, le rapport tend vers la valeur constante ϕ (phi), également appelée le nombre d'or, qui est d'environ 1.6180339887498951")
    entrerContinuer()
    clear()

    



    



from tools.console import *
#4.10.1
def jourDeLaSemaine():
    clear()
    print("Jour de la semaine(en utilisant les indiçages)")
    print()
    jour = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    print("Voici la liste des jours de la semaine :")
    print(jour)
    print()
    #question 1
    print("les 5 premiers jours de la semaine d'une part, et ceux du week-end d'autre part ")
    print("jour[0:5]")
    print(jour[0:5])
    print("jour[5:]")
    print(jour[5:])
    print()
    #question 2
    print("Avec un autre indiçage\nles 5 premiers jours de la semaine d'une part, et ceux du week-end d'autre part")
    print("jour[0:-2]")
    print(jour[0:-2])
    print("jour[-2:]")
    print(jour[-2:])
    print()
    #question 3
    print("Dernier jour de la semaine")
    print()
    #une manière
    print("jour[6]")
    print(jour[6])
    print()
    #une autre manière
    print("jour[-1]")
    print(jour[-1])
    print()
    #question 4
    print("jour de la semaine inversés")
    print("jour[-1:-8:-1]")
    print(jour[-1:-8:-1])
    entrerContinuer()
    clear()

#4.10.2
def saisons():
    clear()
    hiver = ["Novembre","Decembre","Janvier"]
    printemps = ["Fevrier","Mars","Avril"]
    ete = ["Mai","Juin","Juillet"]
    automne = ["Aout","Septembre","Octobre"]

    saisons = [hiver, printemps, ete, automne]
    print("Saisons (indiçage de liste dans une liste)")
    print(f"saisons : {saisons}")
    print()
    print(f"Devinez le resultat de l'instruction : \nsaisons[2]")
    input("Appuyer sur entrer pour voir")
    print(saisons[2])
    input("Appuyer sur entrer pour en deviner d'autres")
    clear()
    print("Saisons (indiçage de liste dans une liste)")
    print(f"saisons : {saisons}")
    print()
    print()
    print(f"Devinez le resultat de l'instruction : \nsaisons[1][0]")
    input("Appuyer sur entrer pour voir")
    print(saisons[1][0])
    input("Appuyer sur entrer pour en deviner d'autres")
    clear()
    print("Saisons (indiçage de liste dans une liste)")
    print(f"saisons : {saisons}")
    print()
    print(f"Devinez le resultat de l'instruction : \nsaisons[1][2]")
    input("Appuyer sur entrer pour voir")
    print(saisons[1][2])
    input("Appuyer sur entrer pour en deviner d'autres")
    clear()
    print("Saisons (indiçage de liste dans une liste)")
    print(f"saisons : {saisons}")
    print()
    print(f"Devinez le resultat de l'instruction : \nsaisons[:][1]")
    input("Appuyer sur entrer pour voir")
    print(saisons[:][1])
    print("\nExplication :\n[:]cela renvoie une nouvelle liste qui contient les mêmes sous-listes que saisons\n\
[1] Cela accède au deuxième élément (index 1) de la liste résultante")
    entrerContinuer()
    clear()
#4.10.3
def table9():
    clear()
    print("Instructions range() et list()\nTable de multiplication par 9")
    print("list(range(0,91,9))")
    print(list(range(0,91,9)))
    entrerContinuer()
    clear()
#4.10.4
def nombrePairs():
    clear()
    print("Instructions len(), range() et list()\nNombres pairs dans [2, 10000]")
    print("len(list(range(2,10001,2)))")
    print(len(list(range(2,10001,2))))
    entrerContinuer()
    clear()
#4.10.5
def listEtIndiceSemaine():
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    clear()
    print("Liste et indice\n")
    print("La liste semaine : ", semaine)
    print()
    print("Valeur de semaine[4] : ", semaine[4])
    print()
    print("Echange de la valeur de la première et de la dernière cases")
    print(f"Avant permutation\nsemaine[0] = {semaine[0]}\nsemaine[6] = {semaine[6]}")
    print()
    print("Voici la permutation :")
    print(f"temp = semaine[0]\n\
semaine[0]=semaine[6]\n\
semaine[6]=temp")
    temp = semaine[0]
    semaine[0]=semaine[6]
    semaine[6]=temp
    print()
    print("Après permutation :")
    print(f"semaine[0] = {semaine[0]}\nsemaine[6] = {semaine[6]}")
    print()
    print("Affichage du dernier élément de la liste 12 fois")
    print("(semaine[6]+\"\n\")*12")
    print((semaine[6]+"\n")*12)
    entrerContinuer()
    clear()
#4.10.6
def lstVidetFloat():
    clear()
    print("list() & range()")
    print()
    print("Création de lstVide et lstFlottant")
    print("lstVide, lstFlottant = list(), [0.0]*5")
    lstVide, lstFlottant = list(), [0.0]*5
    print("Affichage :")
    print("lstVide : ", lstVide)
    print("lstFlottant", lstFlottant)
    print()
    print("ajout des nombres entre 0 et 1000 avec « step » de 200")
    print("lstVide = list(range(0,1001,200))")
    lstVide = list(range(0,1001,200))
    print("lstVide", lstVide)
    print()
    print("Affichaage :")
    print("list(range(0,4)) : ", end="")
    print(list(range(0,4)))
    print("list(range(4,8)) : ", end="")
    print(list(range(4,8)))
    print("list(range(2,9,2)) : ", end="")
    print(list(range(2,9,2)))
    print()
    print("Création de lstElmnt comme une liste des entiers de 0 à 5.")
    print("lstElement = list(range(0,6))")
    lstElement = list(range(0,6))
    print("lstelement = ", lstElement)
    print("Ajout des contenus des deux listes() à la fin de la liste lstElmnt.")
    print("lstElement += lstVide")
    lstElement += lstVide
    print("lstElement += lstFlottant")
    lstElement += lstFlottant
    print()
    print("Affichage")
    print("lstElement = ", lstElement)
    entrerContinuer()
    clear()
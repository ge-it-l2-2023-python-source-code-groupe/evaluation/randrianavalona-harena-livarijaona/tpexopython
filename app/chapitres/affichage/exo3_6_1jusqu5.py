from tools.console import *
def interpreteurEtProgram():
    clear()
    print("Affichage dans l'interpréteur et dans un programme :")
    print()
    print("Quand on tape l'istruction 1+1 dans un interpréteur cela affiche :\n>>> 1 + 1\n2")
    print("En revanche, quand on écrit la même chose dans un script test.py que nous avons créé avec un éditeur de texte\n\
Quand nous avons exécuté ce script en tapant python test.py dans un shell :\n\
Cela n'a rien affiché.\n\
Que se passe t-il ?\n\
- L'interpreteur effectue le calcul et affiche le résultat ce n'est pas le cas de test.py\n\
Pourquoi ?\n\
- Le script s'exécute, mais il ne produit pas de sortie visible dans le terminal.\n\
Pour afficher le résultat :\n\
- Pour voir le résultat il faut utiliser la fonction print() dans le script : print(1+1) cela va afficher 2")
    entrerContinuer()
    clear()

def polyA():
    clear()
    print("Brin d'ADN poly-A(Opération sur les strings)")
    print()
    polyA = "A"*20
    print(polyA)
    entrerContinuer()
    clear()

def polyAetGC():
    clear()
    print("Brin d'ADN Poly-A et poly-GC(Opération sur les strings)")
    print()
    polyA = "A"*20
    polyGC = "GC"*20
    print(f"{polyA}{polyGC}")
    entrerContinuer()
    clear()

def ecritureFormate():
    clear()
    print("Écriture formatée")
    print()
    a = "salut"
    b = 102
    c = 10.318
    print(f"{a} {b} {c:.2f}")
    entrerContinuer()
    clear()

def ecritureFormate2():
    clear()
    print("Écriture formatée 2")
    print()
    perc_CG = ((4500+2575)/14800)*100
    print(f"Le pourcentage de GC est {perc_CG:.0f}     % \n\
Le pourcentage de GC est {perc_CG:.1f}   % \n\
Le pourcentage de GC est {perc_CG:.2f}  % \n\
Le pourcentage de GC est {perc_CG:.3f} %")
    entrerContinuer()
    clear()
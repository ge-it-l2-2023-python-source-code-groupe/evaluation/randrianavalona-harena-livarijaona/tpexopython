from tools.console import *
#6.7.1
def messageBoucle():
    clear()
    print("Affichage de messages en utilisant des test")
    print()
    semaine = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"]
    print(semaine)
    print()
    for day in semaine :
        if day in semaine[:4] : 
        #jour ouvrable     
            print(f"Au travail car c'est {day}")
        #vendredi joli
        elif day == semaine[4] :
            print(f"Chouette c'est {day}")
        #weekend
        elif day in semaine[5:]:
            print(f"Le {day}, je me reposerais ce jour-la...")
    entrerContinuer()
    clear()
#6.7.2
def sequenceCompl():
    clear()
    print("Un script qui transforme une séquence en sa séquence complémentaire")
    brin = ["A", "C", "G", "T", "T", "A", "G", "C", "T", "A", "A", "C", "G"]
    #Affichage du brin 
    for i in range(0, len(brin)):
        print(f"{brin[i]}", end=" ")
    print()
    #la séquence complémentaire s'obtient en remplaçant A par T,TparA,CparG et GparC
    print()
    print("for i in range(len(brin)) :\n\
    if (brin[i] == \"A\"):\n\
        brin[i]=\"T\"\n\
    elif (brin[i] == \"T\"):\n\
        brin[i]=\"A\"\n\
    elif (brin[i] == \"C\"):\n\
        brin[i]=\"G\"\n\
    else :\n\
        brin[i]=\"C\"")
    
    for i in range(len(brin)) :
        if (brin[i] == "A"):
            brin[i]="T"
        elif (brin[i] == "T"):
            brin[i]="A"
        elif (brin[i] == "C"):
            brin[i]="G"
        else :
            brin[i]="C"
    print()
    print("Voici la séquence complémentaire :")
    #Affichage de sa séquence complémentaire
    for i in range(0, len(brin)):
        print(f"{brin[i]}", end=" ")
    
    entrerContinuer()
    clear()

#6.7.3
def minimum():
    clear()
    print("Script qui détermine la valeur le plus petit d'une liste sans utiliser la fonction min()")
    liste = [8, 4, 6, 1, 5]
    min = liste[0]
    for i in range(0, len(liste)):
        print(liste[i], end=" ")

    for i in range(0, len(liste)):
        if(min > liste[i]):
            min=liste[i]
    print()
    print(f"Le minimum dans cette liste est : {min}")
    print()
    print("Voici comment on a fait ")
    print("min = liste[0]\n\
    for i in range(0, len(liste)):\n\
        print(liste[i], end=\" \")\n\n\
    for i in range(0, len(liste)):\n\
        if(min > liste[i]):\n\
            min=liste[i]\n\
    print(f\"Le minimum dans cette liste est : {min}\")")
       
    entrerContinuer()
    clear()

#6.7.4
def frequenceAcideAmine():
    clear()
    print("La fréquence des acides aminés ")
    liste = ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]
    freqA, freqR, freqW, freqG = 0, 0, 0, 0
    for i in range(len(liste)):
        if(liste[i]=="A"):
            freqA += 1
        elif(liste[i]=="R"):
            freqR +=1
        elif(liste[i]=="W"):
            freqW +=1
        else:
            freqG +=1

    for i in range(len(liste)):
        print(liste[i], end=" ")
    print()
    print(f"la fréquence de A est {freqA}\nla fréquence de R est {freqR}\nla fréquence de W est {freqW}\nla fréquence de G est {freqG}")
    entrerContinuer()
    clear()

#6.7.5
def noteMinMax():
    clear()
    print("Un script qui affiche la note maximum la note minimum et qui calcule la moyenne")
    notes = [14, 9, 13, 15, 12]
    print(f"Voici les notes : {notes}")
    print()
    print("On utilise les fonctions min() et max()\nnoteMax = max(notes)\n\
noteMin = min(notes)")
    print()
    noteMax = max(notes)
    noteMin = min(notes)
    print("La note la plus grande est :", noteMax)
    print("La note la plus petite est :", noteMin)
    sommeNote = 0
    print()
    for i in range(len(notes)):
        sommeNote += notes[i]
    moyenne = sommeNote/len(notes)
    print("Moyenne avec deux décimals => {moyenne:.2f}")
    print(f"La moyenne est de {moyenne:.2f}")
    if(moyenne<10):
        print("Recalé")
    elif(moyenne>=10 and moyenne < 12):
        print("Mention Passable")
    elif(moyenne>=12 and moyenne<14):
        print("Mention Assez bien")
    else:
        print("Mention Bien")
    entrerContinuer()
    clear()

#6.7.6
def nombrePairs():
    clear()
    print("Nombres pairs inférieurs ou égaux à 10 d'une part\n\
et les nombres impairs strictement supérieurs à 10 d'autre part")
    for i in range(20):
        if(i<=10 and i%2==0):
            print(i, end=" ")
        elif(i>10 and i%2==1):
            print(i, end=" ")
    entrerContinuer()
    clear()

#6.7.7
def syracuse():
    clear()
    print("un script qui, partant d'un entier positif 'n' crée une liste des nombres de la suite de Syracuse")
    #conjecture de syracuse
    print("Entre un entier positif 'n' comme point de départ")
    n = int(input())
    print("Entrer le nombre de termes que vous souhaitez\nitérations : ")
    iteration = int(input())
    print(f"La suite de Syracuse avec {iteration} itérations : ")
    print(n, end = " ")
    i = 0
    while(i < 20):
        if (n % 2 == 0) : 
            n = int(n / 2)
        elif(n % 2 == 1) :
            n = 3 * n + 1
        print(n, end=" ")
        i+=1
    print()
    print("La conjoncture de Syracus est toujours vérifiée si le nombre d'itération est suffisant")
    print("Le cycle trivial est : 4 2 1")
    entrerContinuer()
    clear()

#6.7.8
def helice():
    clear()
    print("un script qui teste, pour chaque acide aminé, s'il est ou non en hélice et affiche les valeurs des angles phi et psi")
    print()
    phiEtPsi = [[48.6, 53.4],[-124.9, 156.7],[-66.2, -30.8], [-58.8, -43.1],[-73.9, -40.6],[-53.7, -37.5], [-80.6, -26.0],[-68.5, 135.0],[-64.9, -23.5], [-66.9, -45.5],[-69.6, -41.0],[-62.7, -37.5], [-68.2, -38.3],[-61.2, -49.1],[-59.7, -41.1]]
    helice, pasHelice = 0, 0

    for i in range(len(phiEtPsi)):
        for j in range(1,2):
            if(-57-30 <= phiEtPsi[i][j-1] <= -57 + 30 and -47-30 <= phiEtPsi[i][j] <= -47+30):
                print(f"[{phiEtPsi[i][j-1]}], {phiEtPsi[i][j]}] est en hélice")
                helice += 1
            else:
                print(f"[{phiEtPsi[i][j-1]}, {phiEtPsi[i][j]}] n'est pas en hélice")
                pasHelice +=1
    print()
    if helice > pasHelice :
        print("La structure secondaire majoritaire de ces 15 acides aminés sont en helice")
    else :
        print("La structure secondaire majoritaire de ces 15 acides aminés ne sont pas en helice")
    entrerContinuer()
    clear()

#6.7.9
def nombrePremiers():
    clear()
    print("Programme qui détermine les nombres premiers inférieurs à 100 et qui compte combien y a-t-il de nombres premiers entre 0 et 100")
    #methode 1
    print()
    print("Résultat avec Méthode 1 qui consiste à identifier les nombres qui n'ont que deux diviseurs : 1 et lui même")
    nombrePremier = []
    for x in range(2, 101):
            modulo = []
            for y in range(1, (x+1)):
                reste = x%y
                if reste == 0:
                    modulo.append(y)
            if len(modulo) == 2:
                nombrePremier.append(x)    
    print(nombrePremier)
    print()
    #methode 2
    print("Résultat avec Méthode 2 qui consiste à identifier les nombres qui ne sont pas le produit de deux nombre premiers ")
    ispremier = False
    premier = [2]
    
    for i in range(3,100):
        for j in premier:
            if (i % j == 0):
                ispremier=False
                break
            else:
                ispremier=True
        if ispremier:
            #print(i, end = " ")
            premier += [i]
    
    print(premier)
    print()
    print(f"il y a {len(premier)} nombre premier entre 1 et 100")
    print()
    entrerContinuer()
    clear()

#6.7.10
def dichotomie():
    clear()
    print("Pensez à un nombre entre 1 et 100.")
    nbQuestion, min, max = 1, 0, 100
    mid = int((min+max)/2)

    print(f"Est ce que le nombre est plus grand, plus petit ou égal à {mid} ?")
    plusMoins = input("[ + / - / = ] ")
    while plusMoins != "=":
        
        if plusMoins == "+":
            
            min = mid
            mid = int((min+max)/2)
            print(f"Est ce que le nombre est plus grand, plus petit ou égal à {mid} ?")
            plusMoins = input("[ + / - / = ] ")
            nbQuestion +=1
            
        elif plusMoins == "-":
            max= mid
            mid = int((min+max)/2)
            print(f"Est ce que le nombre est plus grand, plus petit ou égal à {mid} ?")
            plusMoins = input("[ + / - / = ] ")
            nbQuestion +=1
            
    if plusMoins == "=":
            nbQuestion +=1
            print(f"Je l'ai trouvé en {nbQuestion} questions")
    entrerContinuer()
    clear()
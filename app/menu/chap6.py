from tools.console import*
from chapitres.test.exo6_7_1jusqu10 import*
def chap6():
    while True:
        print("Chap 6 : ")
        print("6.7.1")
        print("6.7.2")
        print("6.7.3")
        print("6.7.4")
        print("6.7.5")
        print("6.7.6")
        print("6.7.7")
        print("6.7.8")
        print("6.7.9")
        print("6.7.10")
               

        choiceChap6 = input("Appuyer sur 'r' pour retourner au menu principal ou\nSaisissez le numéro d'exo ( ex: 6.7.1) : ")
        if choiceChap6 == "6.7.1":
            messageBoucle()
        elif choiceChap6 == "6.7.2":
            sequenceCompl()
        elif choiceChap6 == "6.7.3":
            minimum()
        elif choiceChap6 == "6.7.4":
            frequenceAcideAmine()
        elif choiceChap6 == "6.7.5":
            noteMinMax()
        elif choiceChap6 == "6.7.6":
            nombrePairs()
        elif choiceChap6 == "6.7.7":
            syracuse()
        elif choiceChap6 == "6.7.8":
            helice()
        elif choiceChap6 == "6.7.9":
            nombrePremiers()
        elif choiceChap6 == "6.7.10":
            dichotomie()
        elif choiceChap6 == 'r':
            clear()
            break
        else:
            clear()
            continue
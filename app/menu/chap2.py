from tools.console import *
from chapitres.variables.exo2_11_1jusqu3 import *
def chap2():
    while True:
        clear()
        print("Chap 2 : ")
        print("2.11.1")
        print("2.11.2 ")
        print("2.11.3")

        choiceChap2 = input("Appuyer sur 'r' pour retourner au menu principal ou\nSaisissez le numéro d'exo ( ex: 2.11.1) : ")
        if choiceChap2 == "2.11.1":
            friedman()
        if choiceChap2 == "2.11.2":
            predireOperation()
        if choiceChap2 == "2.11.3":
            predireConversion()
        elif choiceChap2 == 'r':
            clear()
            break   
        else:
            clear()
            continue
from tools.console import *
from chapitres.affichage.exo3_6_1jusqu5 import *
def chap3():
    while True:
        clear()
        print("Chap 3 : ")
        print("3.6.1")
        print("3.6.2")
        print("3.6.3")
        print("3.6.4")
        print("3.6.5")
        
        

        choiceChap3 = input("Appuyer sur 'r' pour retourner au menu principal ou\nSaisissez le numéro d'exo ( ex: 3.6.1) : ")
        if choiceChap3 == "3.6.1":
            interpreteurEtProgram()
        elif choiceChap3 == "3.6.2":
            polyA()
        elif choiceChap3 == "3.6.3":
            polyAetGC()
        elif choiceChap3 == "3.6.4":
            ecritureFormate()
        elif choiceChap3 == "3.6.5":
            ecritureFormate2()
        elif choiceChap3 == 'r':
            clear()
            break 
        else:
            clear()
            continue
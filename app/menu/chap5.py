from tools.console import *
from chapitres.boucleEtComparaison.exo5_4_1jusqu14 import *
def chap5():
    while True:
        print("Chap 5 : ")
        print("5.4.1")
        print("5.4.2")
        print("5.4.3")
        print("5.4.4")
        print("5.4.5")
        print("5.4.6")
        print("5.4.7")
        print("5.4.8")
        print("5.4.9")
        print("5.4.10")
        print("5.4.11")
        print("5.4.12")
        print("5.4.13")
        print("5.4.14")
               

        choiceChap5 = input("Appuyer sur 'r' pour retourner au menu principal ou\nSaisissez le numéro d'exo ( ex: 5.4.1) : ")
        if choiceChap5 == "5.4.1":
            affichageList()
        elif choiceChap5 == "5.4.2":
            boucleSemaine()
        elif choiceChap5 == "5.4.3":
            unAdixMemeLigne()
        elif choiceChap5 == "5.4.4":
            pairsImpairs()
        elif choiceChap5 == "5.4.5":
            calculMoyenne()
        elif choiceChap5 == "5.4.6":
            produitsConsecutifs()
        elif choiceChap5 == "5.4.7":
            triangle()
        elif choiceChap5 == "5.4.8":
            triangleInverse()
        elif choiceChap5 == "5.4.9":
            triangleGauche()
        elif choiceChap5 == "5.4.10":
            pyramide()
        elif choiceChap5 == "5.4.11":
            parcoursMatrice()
        elif choiceChap5 == "5.4.12":
            affichageDemiMatrice()
        elif choiceChap5 == "5.4.13":
            sautDePuce()
        elif choiceChap5 == "5.4.14":
            fibonacci()
        elif choiceChap5 == 'r':
            clear()
            break
        else:
            clear()
            continue
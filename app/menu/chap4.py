from tools.console import *
from chapitres.listEtIndice.exo4_10_1jusqu6 import *
def chap4():
    while True:
        print("Chap 4 : ")
        print("4.10.1")
        print("4.10.2")
        print("4.10.3")
        print("4.10.4")
        print("4.10.5")
        print("4.10.6")
        

        choiceChap4 = input("Appuyer sur 'r' pour retourner au menu principal ou\nSaisissez le numéro d'exo ( ex: 4.10.1) : ")
        if choiceChap4 == "4.10.1":
            jourDeLaSemaine()
        elif choiceChap4 == "4.10.2":
            saisons()
        elif choiceChap4 == "4.10.3":
            table9()
        elif choiceChap4 == "4.10.4":
            nombrePairs()
        elif choiceChap4 == "4.10.5":
            listEtIndiceSemaine()
        elif choiceChap4 == "4.10.6":
            lstVidetFloat()
        elif choiceChap4 == 'r':
            clear()
            break
        else:
            clear()
            continue
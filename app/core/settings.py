import os
from dotenv import load_dotenv

load_dotenv()

YOUR_ENV_VARIABLE = os.getenv("YOUR_ENV_VARIABLE", "")

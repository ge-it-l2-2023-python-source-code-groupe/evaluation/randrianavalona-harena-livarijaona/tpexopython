from tools.console import *
from menu.chap2 import *
from menu.chap3 import *
from menu.chap4 import *
from menu.chap5 import *
from menu.chap6 import*




def main_menu():
     while True:
        print("Menu principal : ")
        print("2. Chap 2. Variables: de 2.11.1 à 2.11.3")
        print("3. Chap 3. Affichage: de 3.6.1 à 3.6.5")
        print("4. Chap 4. Liste: de 4.10.1 à 4.10.4 + List & indice + 4.10.6 List & range")
        print("5. Chap 5. Boucles et comparaisons: de 5.4.1 à 5.4.14")
        print("6. Chap 6. Test de 6.7.1 à 6.7.10")
        print("q. Quitter")
            
        choice = input("Saisissez le numéro du chapitre : ")
        if choice == 'q' :
            clear()
            exit()
        elif choice == '2':
            clear()
            chap2()  
        elif choice == '3':
            clear()
            chap3()  
        elif choice == '4':
            clear()
            chap4()  
        elif choice == '5':
            clear()
            chap5()  
        elif choice == '6':
            clear()
            chap6()  
        else:
            clear()
            invalide()
if __name__=="__main__":
    
    clear()
    main_menu()
    pass
